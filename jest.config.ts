import type { Config } from 'jest';

const config: Config = {
  preset: 'ts-jest',
  verbose: true,
  moduleDirectories: ['node_modules', 'src'],
  transform: {
    '^.+\\.tsx?$': 'ts-jest'
  }
};

export default config;
