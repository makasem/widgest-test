import { DefineComponent } from 'vue';

export interface UiState {
  isOpenChanges: boolean;
}

export interface Widget {
  name: string,
  component: DefineComponent,
}

export interface WidgetsState {
  allWidgets: Widget[],
  activeWidgets: Widget[],
  unactiveWidgets: Widget[],
}

// weather
export interface WeatherData {
  name: string,
  sys: {
    country: string,
  },
  main: {
    temp: number,
    temp_max: number,
    temp_min: number,
  },
  weather: [
    {
      description: string,
      icon: string,
    }
  ]
}

export interface WeatherState {
  weatherData: WeatherData | {},
}

export interface WeatherPayloadParams {
  lat?: number,
  lon?: number,
  q?: string,
}

export interface WeatherParams {
  appid: string,
  lang: string,
  units: string,
  q?: string,
  lat?: number,
  lon?: number,
}

// time
export interface timeData {
  unixtime: number,
  timezone: string
}

export interface TimeState {
  timeData: timeData,
}

export interface CurrentTime {
  hour: number,
  minute: number,
  second: number,
}

export interface Dial {
  name: string,
  component: DefineComponent,
}

export interface handsSelectors {
  secondHand: string,
  minuteHand: string,
  hourHand: string
}

// course
export interface Valute {
  CharCode: string
  ID: string
  Name: string
  Nominal: number
  NumCode: string
  Previous: number
  Value: number
}

export interface CourseData {
  AUD: Valute
  AZN: Valute
  BGN: Valute
  BRL: Valute
  BYN: Valute
  CAD: Valute
  CHF: Valute
  CNY: Valute
  CZK: Valute
  DKK: Valute
  EUR: Valute
  GBP: Valute
  HKD: Valute
  HUF: Valute
  INR: Valute
  JPY: Valute
  KGS: Valute
  KRW: Valute
  KZT: Valute
  MDL: Valute
  NOK: Valute
  PLN: Valute
  RON: Valute
  SEK: Valute
  SGD: Valute
  TJS: Valute
  TMT: Valute
  TRY: Valute
  UAH: Valute
  USD: Valute
  UZS: Valute
  XDR: Valute
  ZAR: Valute
}

export type ValuteType =
'AUD' |
'AZN' |
'BGN' |
'BRL' |
'BYN' |
'CAD' |
'CHF' |
'CNY' |
'CZK' |
'DKK' |
'EUR' |
'GBP' |
'HKD' |
'HUF' |
'INR' |
'JPY' |
'KGS' |
'KRW' |
'KZT' |
'MDL' |
'NOK' |
'PLN' |
'RON' |
'SEK' |
'SGD' |
'TJS' |
'TMT' |
'TRY' |
'UAH' |
'USD' |
'UZS' |
'XDR';

export interface ResponseCourseData {
  Valute: CourseData
}

export interface CourseState {
  courseData: CourseData
}
