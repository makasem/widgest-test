import { WidgetsState, Widget } from '@/interfaces';
import allWidgets from '@/components/widgets-components-list';
import { cloneDeep } from 'lodash';

export default {
  state: () => ({
    allWidgets,
    unactiveWidgets: cloneDeep(allWidgets),
    activeWidgets: []
  }),

  mutations: {
    setUnactiveWidgets (state: WidgetsState, payload: Widget[]) {
      state.unactiveWidgets = payload;
    },
    setActiveWidgets (state: WidgetsState, payload: Widget[]) {
      state.activeWidgets = payload;
    }
  },

  getters: {
    ALL_WIDGETS: (state: any) => state.allWidgets,
    UNACTIVE_WIDGETS: (state: any) => state.unactiveWidgets,
    ACTIVE_WIDGETS: (state: any) => state.activeWidgets
  }
};
