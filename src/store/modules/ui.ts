import { UiState } from '@/interfaces';

export default {
  state: () => ({
    isOpenChanges: true
  }),

  mutations: {
    setIsOpenChanges: (state: UiState, payload: boolean) => {
      state.isOpenChanges = payload;
    }
  },

  getters: {
    IS_OPEN_CHANGES: (state: UiState) => state.isOpenChanges
  }
};
