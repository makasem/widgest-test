import { TimeState, timeData } from '@/interfaces';
import axios from 'axios';
import dayjs from 'dayjs';

export default {
  state: () => ({
    timeData: {
      timezone: 'europe/moscow',
      unixtime: dayjs().unix()
    }
  }),

  mutations: {
    setTimeData: (state: TimeState, payload: timeData) => {
      state.timeData = payload;
    }
  },

  actions: {
    // TODO сделать тип для commit
    getTimeData: async ({ commit }: any, payload: string) => {
      try {
        const { data } = await axios.get<TimeState>(
          `${process.env.VUE_APP_TIME_URL}/${payload}`
        );
        commit('setTimeData', data);
      } catch (e) {
        console.error(e);
      }
    }
  },

  getters: {
    TIME_DATA: (state: any) => state.timeData
  }
};
