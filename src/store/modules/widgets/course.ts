import axios from 'axios';
import { CourseData, CourseState, ResponseCourseData } from '@/interfaces';

export default {
  state: () => ({
    courseData: {
    }
  }),

  mutations: {
    setCourseData: (state: CourseState, payload: CourseData) => {
      state.courseData = payload;
    }
  },

  actions: {
    // TODO сделать тип для commit
    getCourseData: async ({ commit }: any) => {
      try {
        const { data: { Valute } } = await axios.get<ResponseCourseData>('https://www.cbr-xml-daily.ru/daily_json.js');
        commit('setCourseData', Valute);
      } catch (e) {
        console.error(e);
      }
    }
  },

  getters: {
    COURSE_DATA: (state: CourseState) => state.courseData
  }
};
