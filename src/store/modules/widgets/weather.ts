import { WeatherState, WeatherData, WeatherPayloadParams, WeatherParams } from '@/interfaces';
import axios from 'axios';

export default {
  state: () => ({
    weatherData: {
      weather: [
        {
          description: null,
          icon: ''
        }
      ]
    }
  }),

  mutations: {
    setWeatherData: (state: WeatherState, payload: WeatherData) => {
      state.weatherData = payload;
    }
  },

  actions: {
    // TODO сделать тип для commit
    getWeatherData: async ({ commit }: any, payload: WeatherPayloadParams) => {
      const params: WeatherParams = {
        appid: process.env.VUE_APP_WEATHER_API_KEY,
        lang: 'ru',
        units: 'metric',
        ...payload
      };
      try {
        const { data } = await axios.get<WeatherData>(
          process.env.VUE_APP_WEATHER_URL,
          { params }
        );
        commit('setWeatherData', data);
      } catch (e) {
        console.error(e);
      }
    }
  },

  getters: {
    WEATHER_DATA: (state: any) => state.weatherData
  }
};
