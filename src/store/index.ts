import Vuex, { StoreOptions } from 'vuex';
import ui from './modules/ui';
import widgets from './modules/widgets';
import weather from './modules/widgets/weather';
import time from './modules/widgets/time';
import course from './modules/widgets/course';

// TODO задать интерфейс для стора
const store:StoreOptions<Object> = {
  modules: {
    ui,
    widgets,
    weather,
    time,
    course
  }
};

export default new Vuex.Store<Object>(store);
