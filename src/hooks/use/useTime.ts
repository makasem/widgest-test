import { computed, ref } from 'vue';
import { useStore } from 'vuex';
import utc from 'dayjs/plugin/utc';
import timezone from 'dayjs/plugin/timezone';
import { timeData, CurrentTime } from '@/interfaces';
import dayjs, { Dayjs } from 'dayjs';

dayjs.extend(utc);
dayjs.extend(timezone);

export default () => {
  const store = useStore();
  const timeData = computed<timeData>(() => store.getters.TIME_DATA);
  const serverRequestTime = ref<Dayjs>();

  const getTimeFromServer = (region: string) => {
    serverRequestTime.value = dayjs();
    store.dispatch('getTimeData', region);
  };

  const getCurrentTime = (): CurrentTime => {
    const timeNow = dayjs();
    const diffTime = Math.round(timeNow.diff(serverRequestTime.value) / 1000);
    const currentTime = dayjs.unix(timeData.value.unixtime + diffTime).tz(timeData.value.timezone);
    const second = currentTime.second();
    const minute = currentTime.minute();
    const hour = currentTime.hour();

    return { hour, minute, second };
  };

  return { getTimeFromServer, getCurrentTime };
};
