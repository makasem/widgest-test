import { computed } from 'vue';
import { useStore } from 'vuex';
import { WeatherPayloadParams, WeatherData } from '@/interfaces';

export default () => {
  const store = useStore();

  const getWeatherData = (payload: WeatherPayloadParams) => store.dispatch('getWeatherData', payload);

  const weatherData = computed<WeatherData>(() => store.getters.WEATHER_DATA);

  return { getWeatherData, weatherData };
};
