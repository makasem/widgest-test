// import { computed, ref } from 'vue';
import { ref, computed } from 'vue';
import { DialType, dials } from '@/components/widgets/time-widget/dials';

export default (initialDialName: DialType) => {
  const dialName = ref<DialType>(initialDialName);
  const findDial = (dialName: DialType) => dials.find((dial) => dial.name === dialName);
  const dial = computed(() => findDial(dialName.value));

  const setDial = (payload: DialType) => {
    dialName.value = payload;
  };

  return { setDial, dial };
};
