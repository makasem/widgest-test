import { computed } from 'vue';
import { useStore } from 'vuex';
import { CourseData } from '@/interfaces';

export default () => {
  const store = useStore();
  const courseData = computed<CourseData>(() => store.getters.COURSE_DATA);

  const getCourseData = () => store.dispatch('getCourseData');

  return { courseData, getCourseData };
};
