import {
  defineComponent,
  ref,
  computed,
  onMounted,
  Ref
} from 'vue';
import useWeather from '@/hooks/use/useWeather';
import cx from 'classnames';
// import anime from 'animejs';
import { WeatherPayloadParams } from '@/interfaces';
import './index.scss';

export default defineComponent({
  name: 'weather-widget',

  setup (props) {
    const isOpenSettings = ref<boolean>(false);
    const city: Ref<string> = ref('');

    const { getWeatherData, weatherData } = useWeather();
    const iconURL = computed(() => `http://openweathermap.org/img/wn/${weatherData.value?.weather[0]?.icon}@2x.png`);
    const formateDegrees = (deg: number): string => `${Math.round(deg)}°`;
    const toggleSettings = (): void => {
      isOpenSettings.value = !isOpenSettings.value;
    };

    const handlerSelectCity = () => {
      const params = { q: city.value };
      getWeatherData(params);
    };

    const handlerCity = (e: KeyboardEvent): void => {
      const target = e.target as HTMLInputElement;
      city.value = target.value;
    };

    const getMyGeolocation = () => {
      navigator.geolocation.getCurrentPosition(
        ({ coords: { latitude, longitude } }) => {
          const params: WeatherPayloadParams = {
            lat: latitude,
            lon: longitude
          };
          getWeatherData(params);
        }, (error) => {
          console.log(error);
        });
    };

    // setTimeout(() => {
    //   var textWrapper = document.querySelector('.weather-widget__city');
    //   if (textWrapper instanceof HTMLElement && textWrapper !== null ) {
    //     const splitedHtml = textWrapper.textContent;
    //     if (splitedHtml) {
    //       textWrapper.innerHTML = splitedHtml.replace(/\S/g, "<span class='letter'>$&</span>")
    //     }
    //   }
    //   anime
    //     .timeline({loop: false})
    //     .add({
    //       targets: '.weather-widget__city .letter',
    //       translateX: [40,0],
    //       translateZ: 0,
    //       opacity: [0,1],
    //       easing: "easeOutExpo",
    //       duration: 1200,
    //       delay: (el, i) => 500 + 30 * i
    //     })
    //     // .add({
    //     //   targets: '.weather-widget__city .letter',
    //     //   translateX: [0,-30],
    //     //   opacity: [1,0],
    //     //   easing: "easeInExpo",
    //     //   duration: 1100,
    //     //   delay: (el, i) => 100 + 30 * i
    //     // });
    // }, 2000)

    onMounted(() => {
      // TODO записать в локал сторадж
      city.value = 'Москва';
      const params = { q: city.value };
      getWeatherData(params);
    });

    return () => {
      return (
        <div
          class={cx(
            'weather-widget',
            isOpenSettings.value && 'in-settings'
          )}>
          <div
            class="front-side"
            onClick={ toggleSettings }>
            <div class="weather-widget__city">
              <b>{ weatherData.value?.sys?.country }</b>
              { weatherData.value.name }
            </div>
            <div class="weather-widget__temperature">
              { formateDegrees(weatherData.value?.main?.temp) }
              <img
                class="weather-widget__icon"
                src={iconURL.value}
                draggable="false"/>
            </div>
            <div class="weather-widget__other-temperature">
              <span>{ formateDegrees(weatherData.value?.main?.temp_max) }↑</span>
              <span>{ formateDegrees(weatherData.value?.main?.temp_min) }↓</span>
            </div>
            <div class="weather-widget__weather">
              { weatherData.value?.weather[0]?.description }
            </div>
          </div>
          <div class="back-side">
            <div class="weather-widget__setting-header">
              <button
                class="weather-widget__get-geolocation-btn"
                data-tooltip="Мое местоположение"
                onClick={ getMyGeolocation }>
              </button>
              <button
                class="ui-button ui-button--small"
                onClick={ toggleSettings }>
                готово
              </button>
            </div>
            <div class="ui-label">Город</div>
            <input
              class="ui-input"
              draggable="false"
              type="text"
              value={ city.value }
              onKeyup={ handlerCity }/>
            <button
              class="ui-button"
              onClick={ handlerSelectCity }>
              Выбрать город
            </button>
            <div class="weather-widget__geolocation">
              Текущий город: { weatherData.value.name }
            </div>
          </div>
        </div>
      );
    };
  }
});
