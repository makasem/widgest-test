import {
  defineComponent,
  onMounted,
  ref,
  Ref
} from 'vue';
import useTime from '@/hooks/use/useTime';
import useDials from '@/hooks/use/useDials';
import cx from 'classnames';
import timeZones from './timezones';
import { dials, DialType } from './dials';
import './index.scss';

export default defineComponent({
  name: 'time-widget',

  setup (props) {
    const isOpenSettings: Ref<boolean> = ref(false);
    const { setDial, dial } = useDials('analog');
    const { getTimeFromServer, getCurrentTime } = useTime();

    let timeZone: string = '';

    const toggleSettings = () => {
      isOpenSettings.value = !isOpenSettings.value;
    };

    const heandlerChangeTimeZone = (e: Event) => {
      const target = e.target as HTMLSelectElement;
      timeZone = target.value;
    };

    const handlerSelectTimeZone = () => getTimeFromServer(timeZone);

    const heandlerChangeDial = (e: Event) => {
      const target = e.target as HTMLSelectElement;
      const value = target.value as DialType;
      setDial(value);
    };

    onMounted(() => {
      const defaultTimeZone = 'Europe/Moscow';
      getTimeFromServer(defaultTimeZone);
    });

    return () => {
      return (
        <div
          class={cx(
            'time-widget',
            isOpenSettings.value && 'in-settings'
          )}>
          <div
            class="front-side"
            onClick={toggleSettings}>
            { dial.value && <dial.value.component getCurrentTime={getCurrentTime}/> }
          </div>
          <div class="back-side">
            <div class="time-widget__setting-header">
              <button
                class="ui-button ui-button--small"
                onClick={toggleSettings}>
                готово
              </button>
            </div>
            <div class="ui-label">Регион</div>
            <select
              class="ui-select"
              value={dial}
              onChange={heandlerChangeTimeZone}>
              {
                timeZones.map((timeZone) => (
                  <option value={timeZone}>
                    { timeZone }
                  </option>
                ))
              }
            </select>
            <button
              class="ui-button"
              onClick={handlerSelectTimeZone}>
              Выбрать регион
            </button>
            <div class="ui-label">Тип циферблата</div>
            <select
              class="ui-select"
              onClick={heandlerChangeDial}>
              {
                dials.map(({ name }) => (
                  <option value={name}>
                    { name }
                  </option>
                ))
              }
            </select>
          </div>
        </div>
      );
    };
  }
});
