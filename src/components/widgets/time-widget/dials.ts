import Analog from './dials/analog';
import Digital from './dials/digital';

export type DialType = 'analog' | 'digital';

export const dials = [
  {
    name: 'analog',
    component: Analog
  },
  {
    name: 'digital',
    component: Digital
  }
];
