import {
  defineComponent,
  PropType,
  reactive
} from 'vue';
import gsap from 'gsap';
import { CurrentTime } from '@/interfaces';
import './index.scss';

export default defineComponent({
  name: 'digital-dial',

  props: {
    getCurrentTime: {
      type: Function as PropType<() => CurrentTime>,
      default: () => ({
        second: 0,
        minute: 0,
        hour: 0
      })
    }
  },

  setup (props) {
    const time = reactive<CurrentTime>(props.getCurrentTime());

    gsap.ticker.fps(1);
    gsap.ticker.add(() => {
      const { hour, minute, second } = props.getCurrentTime();
      time.hour = hour;
      time.minute = minute;
      time.second = second;
    });

    return () => {
      return (
        <div class="digital-dial">
          <div>{time.hour}:{time.minute}:{time.second}</div>
        </div>
      );
    };
  }
});
