import {
  defineComponent,

  PropType
} from 'vue';
import { CurrentTime } from '@/interfaces';
import gsap from 'gsap';
import './index.scss';

export default defineComponent({
  name: 'analog-dial',

  props: {
    getCurrentTime: {
      type: Function as PropType<() => CurrentTime>,
      default: () => ({
        second: 0,
        minute: 0,
        hour: 0
      })
    }
  },

  setup (props) {
    gsap.ticker.fps(20);
    gsap.ticker.add(() => {
      const { hour, minute, second } = props.getCurrentTime();

      const startSecond = (second * 6) + 180;
      const startMinute = (minute * 6) + 180;
      const startHour = (hour * 30) + 180 + (minute / 2);

      gsap.set('.analog-dial__hand--second', { x: '-50%', y: 0 });
      gsap.set('.analog-dial__hand--minute', { x: '-50%', y: 0 });
      gsap.set('.analog-dial__hand--hour', { x: '-50%', y: 0 });
      gsap.to('.analog-dial__hand--second', { rotation: startSecond, duration: 0, ease: 'linear' });
      gsap.to('.analog-dial__hand--minute', { rotation: startMinute, duration: 0, ease: 'linear' });
      gsap.to('.analog-dial__hand--hour', { rotation: startHour, duration: 0, ease: 'linear' });
    });

    return () => {
      return (
        <time class="analog-dial">
          <div class="analog-dial__dial-circle">
          </div>
          <div class="analog-dial__hands">
            <div class="analog-dial__hand analog-dial__hand--hour">
            </div>
            <div class="analog-dial__hand analog-dial__hand--minute">
            </div>
            <div class="analog-dial__hand analog-dial__hand--second">
            </div>
          </div>
        </time>
      );
    };
  }
});
