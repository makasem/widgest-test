import {
  defineComponent,
  onMounted,
  ref,
  watch,
  computed
} from 'vue';
import useCourse from '@/hooks/use/useCourse';
import { Valute, ValuteType } from '@/interfaces';
import cx from 'classnames';
import valuteSymbols from './valute-symbols';
import './index.scss';

export default defineComponent({
  name: 'course-widget',

  setup (props) {
    const isOpenSettings = ref<boolean>(false);
    const { courseData, getCourseData } = useCourse();

    const selectedValute = ref<Valute>();

    const toggleSettings = (e: Event): void => {
      isOpenSettings.value = !isOpenSettings.value;
    };

    const handleSelectValute = (e: Event): void => {
      const target = e.target as HTMLSelectElement;
      const value = target.value as ValuteType;
      selectedValute.value = courseData.value[value];
    };

    const valuteSymbol = computed<string>(() => valuteSymbols[selectedValute.value?.CharCode || '']);

    watch(courseData, () => {
      selectedValute.value = courseData.value.USD;
    });

    onMounted(() => {
      getCourseData();
    });

    return () => {
      return (
        <div
          class={cx(
            'course-widget',
            isOpenSettings.value && 'in-settings'
          )}>
          <div
            class="front-side"
            onClick={ toggleSettings }>
              { selectedValute.value &&
                <div>
                  <div class="course-widget__name">{ selectedValute.value.Name }</div>
                  <div
                    class={cx(
                      'course-widget__value',
                      selectedValute.value.Value > selectedValute.value.Previous && 'course-widget__value--up',
                      selectedValute.value.Value < selectedValute.value.Previous && 'course-widget__value--down'
                    )}>
                    { selectedValute.value.Value.toPrecision(3) } <b>{ valuteSymbol.value }</b>
                  </div>
                  <div>{ selectedValute.value.CharCode }</div>
                </div>
              }
          </div>
          <div class="back-side">
            <div class="course-widget__setting-header">
              <button
                class="ui-button ui-button--small"
                onClick={ toggleSettings }>
                готово
              </button>
            </div>
            <div class="ui-label">Выберите валюту</div>
            <select
              class="ui-select"
              onChange={handleSelectValute}>
              { Object.values(courseData.value).map(
                (item) => (
                  <option value={item.CharCode}>{item.CharCode}</option>
                )
              )}
            </select>
          </div>
        </div>
      );
    };
  }
});
