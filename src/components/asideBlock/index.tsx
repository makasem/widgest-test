import { defineComponent, computed, ref } from 'vue';
import './index.scss';
import { useStore } from 'vuex';
import { clickOutside } from '@/utils';

import cx from 'classnames';

export default defineComponent({
  name: 'aside-block',

  setup (props, { slots }) {
    const store = useStore();
    const aside = ref<HTMLDivElement>();
    const isOpenChanges = computed(() => store.getters.IS_OPEN_CHANGES);

    const closeAside = () => store.commit('setIsOpenChanges', false);

    clickOutside(aside, closeAside);

    return () => {
      return (
        <div
          ref={aside}
          class={cx(
            'aside-block',
            isOpenChanges.value && 'aside-block--opened'
          )}>
            {slots.default?.()}
        </div>
      );
    };
  }
});
