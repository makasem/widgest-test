import { defineAsyncComponent } from 'vue';
const allwidgets = [
  {
    id: 0,
    name: 'weather-widget',
    component: defineAsyncComponent(() => import('@/components/widgets/weather-widget'))
  },
  {
    id: 1,
    name: 'time-widget',
    component: defineAsyncComponent(() => import('@/components/widgets/time-widget'))
  },
  {
    id: 2,
    name: 'course-widget',
    component: defineAsyncComponent(() => import('@/components/widgets/course-widget'))
  }
];

export default allwidgets;
