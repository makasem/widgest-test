/* eslint-disable */
import { ComponentCustomProperties } from 'vue'
import { Store } from 'vuex';
import { UiState } from '@/interfaces';
declare module '*.vue' {
  import type { DefineComponent } from 'vue'
  const component: DefineComponent<{}, {}, any>
  export default component
}

declare module '@vue/runtime-core' {
  interface ComponentCustomProperties {
    $store: Store<UiState>
  }
}
