import { Ref } from 'vue';

export const clickOutside = (ref: Ref, callback: () => void) => {
  document.addEventListener('click', (e: Event) => {
    const target = e.target as HTMLElement;

    if (!ref.value?.contains(target)) callback();
  });
};
