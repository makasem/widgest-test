import { defineComponent } from 'vue';
import asideBlock from '@/components/asideBlock';
import widgetsPreviewList from '@/components/widgets-preview-list/index.vue';
import widgetsBlock from '@/components/widgets-block/index.vue';
import '@/styles/index.scss';

export default defineComponent({
  name: 'App',

  components: {
    asideBlock,
    widgetsPreviewList,
    widgetsBlock
  },

  setup (props) {
    return () => {
      return (
        <div>
          <aside-block>
            <widgets-preview-list />
          </aside-block>
          <widgets-block />
        </div>
      );
    };
  }
});
