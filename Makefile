lint:
		npx eslint . --ext js,ts,tsx

lint-fix:
		npx eslint . --ext js,ts,tsx --fix