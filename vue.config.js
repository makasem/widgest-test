const { defineConfig } = require('@vue/cli-service');

module.exports = defineConfig({
  lintOnSave: false,
  css: {
    loaderOptions: {
      sass: {
        additionalData: '@import "@/styles/globals/_variables.scss"; @import "@/styles/globals/_extends.scss";'
      }
    }
  }
});
